<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BitacoraController;
use App\Http\Controllers\FarmaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/',[HomeController::class,'index']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});


route::get('/redirect',[HomeController::class,'redirect']);

route::get('/view_category',[AdminController::class,'view_category']);

route::post('/add_category',[AdminController::class,'add_category']);

route::get('/delete_category/{id}',[AdminController::class,'delete_category']);

route::get('/view_supplier',[AdminController::class,'view_supplier']);

route::post('/add_supplier',[AdminController::class,'add_supplier']);

route::get('/delete_supplier/{id}',[AdminController::class,'delete_supplier']);

route::get('/view_product',[AdminController::class,'view_product']);

route::post('/add_product',[AdminController::class,'add_product']);

route::get('/show_product',[AdminController::class,'show_product']);

route::get('/delete_product/{id}',[AdminController::class,'delete_product']);

route::get('/update_product/{id}',[AdminController::class,'update_product']);

route::post('/update_product_confirm/{id}',[AdminController::class,'update_product_confirm']);

route::get('/product_details/{id}',[HomeController::class,'product_details']);

route::post('/add_cart/{id}',[HomeController::class,'add_cart']);

route::get('/show_cart',[HomeController::class,'show_cart']);

route::get('/show_admincart', [AdminController::class,'show_admincart']);

Route::post('/add_admincart/{id}', [HomeController::class,'add_admincart']);

Route::get('/admin_preorder', [AdminController::class,'admin_preorder']);

route::get('/remove_cart/{id}',[HomeController::class,'remove_cart'])->name('cart.remove');

route::get('/cash_order',[HomeController::class,'cash_order']);

route::get('/stripe/{totalprice}', [HomeController::class,'stripe']);

Route::post('/stripe/{totalprice}', [HomeController::class,'stripePost'])->name('stripe.post');

Route::get('/delivery_status/{id}',[AdminController::class,'delivery_status']);

Route::get('/stock_level/{id}',[AdminController::class,'stock_level']);

Route::get('/bitacora/export', [BitacoraController::class, 'export']);

Route::get('/bitacora',[BitacoraController::class,'index'])->name('bitacora.index');

Route::get('/bitacora/delete/{id}',[BitacoraController::class,'destroy'])->name('bitacora.boa');

Route::get('/farma',[FarmaController::class,'index'])->name('farma.index');

Route::get('/hide/{id}', [AdminController::class,'hide']);
