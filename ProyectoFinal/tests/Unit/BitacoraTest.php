<?php

namespace Tests\Feature;

use App\Models\Bitacora;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BitacoraTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_delete_a_bitacora_record()
    {
        $bitacora = Bitacora::create([
            'user_name' => 'John Doe',
            'invoice' => 'INV-001',
        ]);
        $response = $this->get(route('bitacora.boa', $bitacora->id));

        $this->assertDatabaseMissing('bitacoras', [
            'id' => $bitacora->id,
        ]);

        $response->assertRedirect(route('bitacora.index'));
        $response->assertSessionHas('success', 'Bitacora deleted successfully');
    }
}
