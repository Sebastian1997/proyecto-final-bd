<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserDetail
 *
 * @property $id
 * @property $details
 * @property $age
 * @property $user_id
 * @property $user_neighborhoods_id
 * @property $created_at
 * @property $updated_at
 *
 * @property UserNeighborhood $userNeighborhood
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserDetail extends Model
{
    
    static $rules = [
		'details' => 'required',
		'age' => 'required',
		'user_id' => 'required',
		'user_neighborhoods_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['details','age','user_id','user_neighborhoods_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userNeighborhood()
    {
        return $this->hasOne('App\Models\UserNeighborhood', 'id', 'user_neighborhoods_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    

}
