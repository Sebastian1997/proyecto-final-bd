<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\Orderproduct;
use App\Models\Supplier;
use App\Models\User;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;

class AdminController extends Controller
{
    //
    public function view_category(){

        $data=Category::all();
        return view('admin.category', compact('data'));
    }
    public function add_category(Request $request){
        $data=new category;

        $data->category_name=$request->category;

        $data->save();

        return redirect()->back()->with('message','Categoria añadida con exito');
    }

    public function delete_category($id){
        $data=Category::find($id);
        $data->delete();
        return redirect()->back()->with('message','Categoria eliminada con exito');
    }

    public function view_supplier(){
        $suppliers=Supplier::all();
        return view('admin.supplier', compact('suppliers'));
    }

    public function add_supplier(Request $request){
        $supplier=new Supplier;

        $supplier->name=$request->supplier_name;

        $supplier->save();

        return redirect()->back()->with('message','Proveedor añadido con exito');
    }

    public function delete_supplier($id){
        $data=Supplier::find($id);
        $data->delete();
        return redirect()->back()->with('message','Proveedor eliminado con exito');
    }

    public function view_product(){
        $category = Category::all();
        $suppliers = Supplier::all();
        return view('admin.product', compact('category', 'suppliers'));
    }

    public function add_product(Request $request){
        $product = new Product;
        
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount_price = $request->dis_price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category;
        $product->supplier_id = $request->supplier;
        $product->supplier_price = $request->sup_price;

        $image=$request->image;
        $imagename=time().'.'.$image->getClientOriginalExtension();
        $request->image->move('product', $imagename);
        $product->image=$imagename;

        $product->save();

        return redirect()->back()->with('message','Producto añadido con exito');
    }

    public function show_product(){
        $products = Product::all();
        return view('admin.show_product', compact('products'));
    }

    public function delete_product($id){
        $data=Product::find($id);
        $data->delete();
        return redirect()->back()->with('messagedelete','Producto eliminado con exito');
    }

    public function update_product($id){
        $producto=Product::find($id);
        $category = Category::all();
        return view('admin.update_product', compact('producto','category'));
    }

    public function update_product_confirm(Request $request, $id){
    
        $product=Product::find($id);
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discount_price = $request->dis_price; 
        $product->category_id = $request->category;
        $product->quantity = $request->quantity;

        if($request->image != null){
        $image=$request->image;
        $imagename=time().'.'.$image->getClientOriginalExtension();
        $request->image->move('product', $imagename);
        $product->image=$imagename;
        }
        $product->save();

        return redirect()->back()->with('messageupdate','Producto actualizado con exito');
     
    }

    public function delivery_status($id){
        $data=Order::find($id);

        if($data->delivery_status=='processing'){
            $data->delivery_status ='delivered';
            $data->save();
            return redirect()->back()->with('message','Estado de entrega actualizado con exito');
        }
        else{
            $data->delivery_status='processing';
            $data->save();
            return redirect()->back()->with('message','Estado de entrega actualizado con exito');
        }

    }

    public function stock_level($id){
        $data=Order::find($id);
        $orders = Orderproduct::where('order_id', $data->id)->get();

        if($data->delivery_status=='processing'){
            $data->delivery_status ='delivered';
            $data->save();

            foreach($orders as $order){
                $product = Product::find($order->product_id);
                $product->quantity = $product->quantity + $order->quantity;
                $product->save();
            }

            return redirect()->back()->with('message','Estado de entrega actualizado con exito');
        }
        else{
            $data->delivery_status='processing';
            $data->save();
            return redirect()->back()->with('message','Estado de entrega actualizado con exito');
        }

    }

    public function show_admincart(){
        $product=Product::paginate(3);
        return view('home.admincart', compact('product'));

    }

    public function admin_preorder(){
        if(Auth::id()){
            $user = User::find(Auth::id());
            $carts = Cart::where('user_id', $user->id)->get();
            return view('home.adminpreorder', compact('carts'));
            }
            else{
                return redirect('login');
            }

    }

    public function hide ($id){
        $order = Order::find($id);
        $order->saved = '1';
        $order->save();

        return redirect()->back()->with('message','Pedido ocultado con exito');
    }

}

