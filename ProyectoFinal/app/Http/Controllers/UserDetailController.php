<?php

namespace App\Http\Controllers;

use App\Models\UserDetail;
use Illuminate\Http\Request;

/**
 * Class UserDetailController
 * @package App\Http\Controllers
 */
class UserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userDetails = UserDetail::paginate();

        return view('user-detail.index', compact('userDetails'))
            ->with('i', (request()->input('page', 1) - 1) * $userDetails->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userDetail = new UserDetail();
        return view('user-detail.create', compact('userDetail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(UserDetail::$rules);

        $userDetail = UserDetail::create($request->all());

        return redirect()->route('user-details.index')
            ->with('success', 'UserDetail created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userDetail = UserDetail::find($id);

        return view('user-detail.show', compact('userDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userDetail = UserDetail::find($id);

        return view('user-detail.edit', compact('userDetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  UserDetail $userDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDetail $userDetail)
    {
        request()->validate(UserDetail::$rules);

        $userDetail->update($request->all());

        return redirect()->route('user-details.index')
            ->with('success', 'UserDetail updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $userDetail = UserDetail::find($id)->delete();

        return redirect()->route('user-details.index')
            ->with('success', 'UserDetail deleted successfully');
    }
}
