<section class="product_section layout_padding">
    <div class="container">
       <div class="heading_container heading_center">
          <h2 style="color: #7ED957">
             Productos
          </h2>
       </div>
       <div class="row">

         @foreach($product as $products)
          <div class="col-sm-6 col-md-4 col-lg-4">
             <div class="box">
                <div class="option_container">
                   <div class="options">
                      
                      <form action="{{url('add_admincart', $products->id)}}" method="POST">
                        @csrf

                        <div class="row">
                           <div class="col-md-4">
                              <input type="number" name="quantity" value="1" min="1"
                              style="width: 100px">
                           </div>
                           <div class="col-md-4">
                              <input type="submit" value="Añadir al carrito" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                           </div>
                        </div>
                     </form>



                   </div>
                </div>
                <div class="img-box">
                   <img src="product/{{$products->image}}" alt="">
                </div>
                <div class="detail-box">
                   <h5>
                      {{$products->title}}
                   </h5>

                   
                   @if($products->discount_price!=null)

                   <h6 style=" color: 
                       blue;">
                        Precio unitario
                     <br>

                     ${{$products->supplier_price}}
                   </h6>

                   @else
                   <h6 style="color: blue;">
                     Precio
                     ${{$products->price}}
                   </h6>
                   @endif

                </div>
             </div>
           </div>
       
         @endforeach

      <span style="padding-top: 20px">

         {!!$product->withQueryString()->links('pagination::bootstrap-5')!!}

      </span>

    </div>
 </section>