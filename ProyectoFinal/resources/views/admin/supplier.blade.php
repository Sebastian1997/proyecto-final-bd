<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    @include('admin.css')

    <style type="text/css">
        .div_center{
          text-align: center;
          padding-top: 40px;
        }
        .h2_font{
          font-size: 40px;
          padding-bottom: 40px;
        }
  
        .input_color{
          color: black;
        }
  
        .center{
          margin: auto;
          width: 50%;
          text-align: center;
          margin-top: 30px;
          border: 3px solid white;
        }
      </style>
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.header')

      @include('admin.script')
    <!-- End custom js for this page -->

    <div class="main-panel">
        <div class="content-wrapper">

            @if(Session::has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">x</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            <div class="div_center">
                <h2 class="h2_font">Añadir Proveedor</h2>

                <form action="{{ url('/add_supplier') }}" method="POST">
                    @csrf

                    <input class="input_color" type="text" name="supplier_name" placeholder="Escribe el nombre del proveedor">

                    <input type="submit" name="submit" class="btn btn-primary"
                    value="Añadir proveedor">
                </form>
            </div>

            <table class="center">
                <tr>
                  <td>Nombre del proveedor</td>
                  <td>Accion</td>
                </tr>
  
                @foreach($suppliers as $supplier)
                <tr>
                  <td>{{$supplier->name}}</td>
                  <td>
                    <a onclick="return confirm('Estas seguro de eliminar al proveedor')" class="btn btn-danger" href="{{url('/delete_supplier', $supplier->id)}}">Eliminar
                    </a>
                  </td>
                </tr>
  
                @endforeach
  
              </table>

        </div>
    </div>

  </body>
</html>