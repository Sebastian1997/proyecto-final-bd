<div class="main-panel">
    <div class="content-wrapper">




      <div class="row ">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Status de ventas </h4>
              
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>
                        <div class="form-check form-check-muted m-0">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input">
                          </label>
                        </div>
                      </th>
                      <th> Nombre del cliente</th>
                      <th> Folio Orden </th>
                      <th> Total </th>
                      <th> Pago </th>
                      <th> Fecha de pedido </th>
                      <th> Estatus del pedido </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orders as $order)
                    @if($order->transaction_type == 0)
                    @if($order->saved != 1)
                    <tr>
                      <td>
                        <div class="">
                          <a href="{{url('/hide', $order->id)}} class="btn btn-primary">Quitar</a>
                        </div>
                      </td>
                      <td>
                        <span class="pl-2">{{$order->user->name}}</span>
                      </td>
                      <td> {{$order->invoice}} </td>
                      <td> {{$order->total}} </td>
                      <td> {{$order->payment_status}} </td>
                      <td> {{$order->created_at}} </td>
                      
                      <td>
                        <a href="{{url('/delivery_status', $order->id)}}">
                        @if($order->delivery_status == "processing")
                        <div class="badge badge-outline-warning">Pendiente</div>
                        @elseif($order->delivery_status == "delivered")
                        <div class="badge badge-outline-primary">En camino</div>
                        @endif
                        </a>
                      </td>
                    </tr>

                    @endif
                    @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>






      <div class="row ">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Status de compras </h4>
              
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>
                        <div class="form-check form-check-muted m-0">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input">
                          </label>
                        </div>
                      </th>
                      <th> Nombre del cliente</th>
                      <th> Folio Orden </th>
                      <th> Total </th>
                      <th> Pago </th>
                      <th> Fecha de pedido </th>
                      <th> Estatus del pedido </th>
                    </tr>
                  </thead>
                  <tbody>
              
                    @foreach($orders as $order)
                    @if($order->transaction_type == 1)
                    @if($order->saved != 1)
                    <tr>
                      <td>
                        <div class="">
                          <a href="{{url('/hide', $order->id)}} class="btn btn-primary">Quitar</a>
                        </div>
                      </td>
                      <td>
                        <span class="pl-2">{{$order->user->name}}</span>
                      </td>
                      <td> {{$order->invoice}} </td>
                      <td> {{$order->total}} </td>
                      <td> {{$order->payment_status}} </td>
                      <td> {{$order->created_at}} </td>
                      
                      <td>
                        <a href="{{url('/stock_level', $order->id)}}">
                        @if($order->delivery_status == "processing")
                        <div class="badge badge-outline-warning">Pendiente</div>
                        @elseif($order->delivery_status == "delivered")
                        <div class="badge badge-outline-primary">Entregado</div>
                        @endif
                        </a>
                      </td>
                    </tr>

                    @endif
                    @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
      
      <div class="row">
        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>Total de usuarios</h5>
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    <h2 style="margin-left: 55px;" class="mb-0">{{$totalusers}}</h2>
                  </div>
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-monitor text-success ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>Total de productos</h5>
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    <h2 style="margin-left: 55px;" class="mb-0">{{$totalproducts}}</h2>
                    
                  </div>
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-codepen text-primary ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>Stock bajo <button class="btn btn-primary">
                
                <a style="color: white;" href="/show_admincart">
                  Pedir
                </a>
                </button></h5> 
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  @foreach($lowstock as $product)
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    
                    <h2 class="mb-0">{{$product->title}}</h2>
                    <p class="text-danger ml-2 mb-0 font-weight-medium">{{$product->quantity}} unidades</p>
                    
                  </div>
                  @endforeach
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-wallet-travel text-danger ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              
                <h4 class="card-title">Transaction History</h4>
                <canvas id="lineChart" style="height:250px"></canvas>


              </div>
            </div>
          </div>
  
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">

              <h4 class="card-title">Transaction History</h4>
              <canvas id="chart"></canvas>

            </div>
          </div>
        </div>
      </div>




    







      
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin templates</a> from Bootstrapdash.com</span>
      </div>
    </footer>
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
