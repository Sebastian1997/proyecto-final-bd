<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    @include('admin.css')

    <style type="text/css">
    
        .div_center
        {
            text-align: center;
            padding-top:40px;
        }
    
        .font_size
        {
            font-size: 40px;
            padding-bottom: 40px;
        }
    
        .text_color{
            color: black;
            padding-bottom: 20px; 
        }
    
        label{
            display: inline-block;
            width: 200px;
        }
    
        .div_design{
            padding-bottom: 15px;
        }
    
        </style>
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')

      @include('admin.header')

      <div class="main-panel">
        <div class="content-wrapper">

          @if(Session::has('message'))
          <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"
              aria-hidden="true">x</button>
              {{ Session::get('message') }}
          </div>
          @endif

            <div class="div_center">
                <h1 class="font_size">Añadir productos</h1>

                <form action="{{url('/add_product')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="div_design">
                <label>Nombre del producto</label>
                <input class="text_color" type="text" name="title" 
                placeholder="Escribe el nombre del producto" required="">
                </div>

                <div class="div_design">
                <label>Descripcion del producto</label>
                <input class="text_color" type="text" name="description" 
                placeholder="Escribe la descripcion del producto" required="">
                </div>

                <div class="div_design">
                <label>Precio del producto</label>
                <input class="text_color" type="number" name="price" 
                placeholder="Escribe el precio del producto" required="">
                </div>

                <div class="div_design">
                  <label>Precio con descuento</label>
                  <input class="text_color" type="number" name="dis_price" 
                  placeholder="Escribe el precio con descuento">
                </div>

                <div class="div_design">
                  <label>Precio de proveedor</label>
                  <input class="text_color" type="number" name="sup_price" 
                  placeholder="Escribe el precio de proveedor">
                </div>

                <div class="div_design">
                <label>Cantidad del producto</label>
                <input class="text_color" type="number" name="quantity" min="0"
                placeholder="Escribe la cantidad del producto" required="">
                </div>



                <div class="div_design">
                  <label>Categoria del producto</label>
                  <select class="text_color" name="category" required="">
                    <option value="" selected>Añade una categoria </option>
                    @foreach($category as $cat)
                    <option value="{{$cat->id}}">{{$cat->category_name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="div_design">
                  <label>Proveedor del producto</label>
                  <select class="text_color" name="supplier" required="">
                    <option value="" selected>Añade un proveedor </option>
                    @foreach($suppliers as $sup)
                    <option value="{{$sup->id}}">{{$sup->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="div_design">
                  <label>Imagen del producto</label>
                  <input type="file" name="image" required="">
                </div>

                <div class="div_design">
                  <input class="btn btn-primary" value="Añadir producto" type="submit">
                </div>
                
                </form>

            </div>

        </div>
    </div>

      @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>