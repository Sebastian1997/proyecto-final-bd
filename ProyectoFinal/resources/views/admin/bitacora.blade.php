<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    @include('admin.css')

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-s2Q7UuUC6+YV6JpH6dDML6p+9lAP1AWCY4K4dCcJ0x5u+oYwQ7ODh5c5ZJDRBQtf/Ye70MEh+Dv0b8zWtYgsLw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.header')
        <!-- partial -->
      @include('admin.bitacoraview')
        <!-- partial -->
    <!-- container-scroller -->
    <!-- plugins:js -->
      @include('admin.script')
    <!-- End custom js for this page -->

  </body>
</html>