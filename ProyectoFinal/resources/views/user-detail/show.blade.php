@extends('layouts.app')

@section('template_title')
    {{ $userDetail->name ?? "{{ __('Show') User Detail" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} User Detail</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('user-details.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Details:</strong>
                            {{ $userDetail->details }}
                        </div>
                        <div class="form-group">
                            <strong>Age:</strong>
                            {{ $userDetail->age }}
                        </div>
                        <div class="form-group">
                            <strong>User Id:</strong>
                            {{ $userDetail->user_id }}
                        </div>
                        <div class="form-group">
                            <strong>User Neighborhoods Id:</strong>
                            {{ $userDetail->user_neighborhoods_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
