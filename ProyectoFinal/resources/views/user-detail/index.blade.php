@extends('layouts.app')

@section('template_title')
    User Detail
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('User Detail') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('user-details.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Details</th>
										<th>Age</th>
										<th>User Id</th>
										<th>User Neighborhoods Id</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($userDetails as $userDetail)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $userDetail->details }}</td>
											<td>{{ $userDetail->age }}</td>
											<td>{{ $userDetail->user_id }}</td>
											<td>{{ $userDetail->user_neighborhoods_id }}</td>

                                            <td>
                                                <form action="{{ route('user-details.destroy',$userDetail->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('user-details.show',$userDetail->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('user-details.edit',$userDetail->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $userDetails->links() !!}
            </div>
        </div>
    </div>
@endsection
